console.log('Hello, World!');

// ES  = ECMA scripts
// ES6 = ECMAScript 2015
	// ES6 udpate brings new features to JS

// 1. Exponent Operator
	// before
	const firstNum = Math.pow(8, 2);

	// after
	const secondNum = 8 ** 2;

// 2. Template Literals
	// Allows us to write strings without using the concatenation operator (+)
	// Uses backticks(``) ${expression}
	const intersetRate = .1;
	const principal = 1000;

	console.log(`The interest on your savings account is ${principal* intersetRate}`);


// 3. Array Destructuring
	// Allows us to unpack elements in arrays into a distinct variables.
	// Syntax:
		// let const [variableNameA, variableNameB, variableNameC] = array;

	const fullName = ['Juan','Dela','Cruz'];

	// Pre-array Destructuring
	console.log(fullName[0]);
	console.log(fullName[1]);
	console.log(fullName[2]);

	console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);


	// Array Destructuring
	const [firstName, middleName, lastName] = fullName;
	console.log(firstName);
	console.log(middleName);
	console.log(lastName);
	
	console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`);
		// ^ Helps with code readability



// 4. Object Destructuring
	// Allows us to unpack properties of objects into distinct variables.
	// Syntax:
		// let/const {propertyNameA, propertyNameB, propertyNameC} = array;

	const person = {
		givenName: 'Jane', 
		maidenName: 'Dela',
		familyName: 'Cruz'
	} 

	// Pre-Object Destructuring
	console.log(person.givenName);
	console.log(person.maidenName);
	console.log(person.familyName);

	console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`)

	// Object Destructuring
	const {givenName, maidenName, familyName} = person;
	console.log(givenName);
	console.log(maidenName);
	console.log(familyName);
		// ^ Shortens the syntax for accessing the properties from objects
		// console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again!`)

	function getFullName({givenName, maidenName, familyName}) {
		console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you`)
	}
	getFullName(person);

		// function getFullName(person) {
		// 	console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you`)
		// }
		// getFullName(person);


// 5. Arrow Function
	// Traditional Functions
	function printFullName(firstName, middleName, lastName) {
		console.log(`${firstName} ${middleName} ${lastName}`)
	}
	printFullName('John','Dela','Cruz');

	// Arrow Function
		/*
			Syntax:
				let/const variableName = (parameterA, parameterB, parameterC) => {
					statement
				}
		*/
	let printFullname1 = (firstName, middleName, lastName) => {
		console.log(`${firstName} ${middleName} ${lastName}`)

	}
	printFullname1('John','D','Smith');


	// Convert (pre-arrow function)
	const students = ['John','Jane','Jasmine'];
	students.forEach(function(student) {
		console.log(`${student} is a student.`)
	})

	// Use Arrow function
	students.forEach((student) => {
		console.log(`${student} is a student.`)
	})
		// ^ We can ommit the parenthesis in the arrow function if we only have one paramter.

	// Implicit Return Statement
		// There are instances when you can omit the "return" statement.

		// Pre-Arrow  Function
		const add = (x, y) => {
			return x + y
		}
		let total = add(1, 2);
		console.log(total);

		// Arrow Function
		const add1 = (x, y) => x + y;
		let total1 = add1(2, 2);
		console.log(total1);

	// Default Function Argument Value
		// Provide a default argument value if none is provided when the function is invoked.
		const greet = (name = 'User') => {
			return `Good morning, ${name}!`
		}
		console.log(greet());


	// Class-Based Object Blueprints
		// Allows creation of objects using classes as blueprints
		// Creating a class
			// constructor is a special method of a class for creating an object
			// this keyword that refers to the properties of an ojbect created from the class.
	class Car {
		constructor(brand, name, year) {
			this.brand = brand;
			this.name = name;
			this.year = year;
		}
	}

	const myCar = new Car();
	console.log(myCar);

	myCar.brand = 'Ford';
	myCar.name = 'Ranger Raptor';
	myCar.year = 2021;
	console.log(myCar);

	const myNewCar = new Car('Toyota','Vios', 2021);
	console.log(myNewCar);


	// Ternary Operator
	// (condition) ? truth : falsy